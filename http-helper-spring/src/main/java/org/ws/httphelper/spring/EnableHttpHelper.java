package org.ws.httphelper.spring;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.ws.httphelper.spring.registrar.HttpOperationInterfaceRegistrar;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@EnableAutoConfiguration
@Import({HttpOperationInterfaceRegistrar.class})
@ComponentScan(basePackages = {"org.ws.httphelper"})
public @interface EnableHttpHelper {
}
