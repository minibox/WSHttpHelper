package org.ws.httphelper.template;

import org.ws.httphelper.builder.RequestTemplateBuilder;
import org.ws.httphelper.model.template.RequestTemplate;

/**
 * 默认的请求模版
 */
public class DefaultRequestTemplate {

    /**
     * OkHttpClient客户端的GET请求HTML的模版。
     * 适用于GET方法的网页请求
     * @return
     */
    public static RequestTemplate okHttpGetHtml(){
        return okHttpGetHtml(null);
    }

    /**
     * 有rootURL的OkHttpClient客户端的GET请求HTML的模版
     * @param rootUrl
     * @return
     */
    public static RequestTemplate okHttpGetHtml(String rootUrl){
        return RequestTemplateBuilder.builder()
                // 默认okHttpClientConfig，异步4线程，超时3s
                .clientConfig(DefaultClientConfig.okHttpClientConfig(4,3_000))
                // 默认getHtml请求配置
                .requestConfig(DefaultRequestConfig.getHtml(rootUrl))
                .build();
    }

    /**
     * WebClient客户端的GET请求HTML的模版。
     * 适用于GET方法的需执行JS渲染的HTML请求。
     * @return
     */
    public static RequestTemplate webGetHtml(){
        return webGetHtml(null);
    }

    /**
     * 有rootUrl的WebClient客户端的GET请求HTML的模版
     * @param rootUrl
     * @return
     */
    public static RequestTemplate webGetHtml(String rootUrl){
        return RequestTemplateBuilder.builder()
                // 默认webClientConfig，异步4线程，超时3s
                .clientConfig(DefaultClientConfig.webClientConfig(4,3_000))
                // 默认getHtml请求配置
                .requestConfig(DefaultRequestConfig.getHtml(rootUrl))
                .build();
    }

    /**
     * OkHttpClient客户端的POST请求JSON的模版。
     * 适用于POST请求JSON的REST接口
     * @return
     */
    public static RequestTemplate okHttpPostJson(){
        return okHttpPostJson(null);
    }

    /**
     * 有rootUrl的OkHttpClient客户端的POST请求JSON的模版
     * @param rootUrl
     * @return
     */
    public static RequestTemplate okHttpPostJson(String rootUrl){
        return RequestTemplateBuilder.builder()
                // 默认okHttpClientConfig，异步4线程，超时3s
                .clientConfig(DefaultClientConfig.okHttpClientConfig(4,3_000))
                // 默认postJson请求配置
                .requestConfig(DefaultRequestConfig.postJson(rootUrl))
                .build();
    }

    /**
     * OkHttpClient客户端的POST请求XML的模版
     * 适用于POST请求XML的接口
     * @return
     */
    public static RequestTemplate okHttpPostXml(){
        return okHttpPostXml(null);
    }

    /**
     * 有rootUrl的OkHttpClient客户端的POST请求XML的模版
     * @return
     */
    public static RequestTemplate okHttpPostXml(String rootUrl){
        return RequestTemplateBuilder.builder()
                // 默认okHttpClientConfig，异步4线程，超时3s
                .clientConfig(DefaultClientConfig.okHttpClientConfig(4,3_000))
                // 默认postXml请求配置
                .requestConfig(DefaultRequestConfig.postXml(rootUrl))
                .build();
    }

}
