package org.ws.httphelper.support.parser.field;


import org.ws.httphelper.exception.ParseException;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.support.parser.field.operator.AbstractParseOperator;
import org.ws.httphelper.support.parser.field.operator.CssQueryParseOperator;
import org.ws.httphelper.support.parser.field.operator.RegexParseOperator;
import org.ws.httphelper.support.parser.field.operator.XPathParseOperator;

import java.util.EnumMap;
import java.util.Map;

/**
 * 描述:  ${DESCRIPTION}
 *
 * @author gz
 * @create 2018-11-15 15:54
 */
public class ParseOperatorFactory {

    private static Map<ExpressionType, AbstractParseOperator> fieldParseOperatorMap = new EnumMap<>(ExpressionType.class);
    private ParseOperatorFactory(){
        fieldParseOperatorMap.put(ExpressionType.CSS,new CssQueryParseOperator());
        fieldParseOperatorMap.put(ExpressionType.XPATH,new XPathParseOperator());
        fieldParseOperatorMap.put(ExpressionType.REGEX,new RegexParseOperator());
    }

    private static ParseOperatorFactory instance = new ParseOperatorFactory();
    public static ParseOperatorFactory getInstance(){
        return instance;
    }

    public AbstractParseOperator getParseOperator(ExpressionType expressionType) throws ParseException {
        //
        if(!fieldParseOperatorMap.containsKey(expressionType)){
            throw new ParseException("Error! Not contains ExpressionType " + expressionType + ".");
        }

        return fieldParseOperatorMap.get(expressionType);
    }

}
