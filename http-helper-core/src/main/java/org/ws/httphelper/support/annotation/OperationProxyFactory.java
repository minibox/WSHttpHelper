package org.ws.httphelper.support.annotation;

import com.google.common.collect.Maps;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

/**
 *
 * @param <T>
 */
public class OperationProxyFactory<T> {

    private final Class<T> operationInterface;
    private final Map<Method,OperationMethod> operationMethodMap = Maps.newConcurrentMap();

    public OperationProxyFactory(Class<T> operationInterface) {
        this.operationInterface = operationInterface;
    }

    public T newInstance(){
        OperationProxy<T> operationProxy = new OperationProxy<>(operationInterface,operationMethodMap);
        return (T) Proxy.newProxyInstance(operationInterface.getClassLoader(),
            new Class[]{operationInterface},operationProxy);
    }

}
