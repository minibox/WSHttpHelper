package org.ws.httphelper.support.pipeline.handler.response;

import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;

@HandlerOrder(0)
public class ExportCsvHandler implements ResponseHandler {
    @Override
    public boolean handler(RequestContext requestContext) throws ResponseException {
        return true;
    }

}
