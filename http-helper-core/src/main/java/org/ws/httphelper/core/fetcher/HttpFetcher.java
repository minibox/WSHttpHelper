package org.ws.httphelper.core.fetcher;

import org.ws.httphelper.exception.FetchException;
import org.ws.httphelper.model.http.RequestData;
import org.ws.httphelper.model.http.ResponseData;
import org.ws.httphelper.model.http.ResponseFuture;

import java.util.function.Consumer;

/**
 * 抓取器：获取Http资源客户端
 */
public interface HttpFetcher {

    /**
     * 执行请求
     * @param requestData
     * @return
     * @throws Exception
     */
    ResponseData fetch(RequestData requestData)throws FetchException;

    /**
     * 异步执行
     * @param requestData
     * @param callback
     * @throws FetchException
     */
    void fetch(RequestData requestData, Consumer<ResponseFuture> callback)throws FetchException;
}
