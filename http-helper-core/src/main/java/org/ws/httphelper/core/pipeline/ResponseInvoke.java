package org.ws.httphelper.core.pipeline;

import org.ws.httphelper.model.RequestContext;

/**
 * 执行响应处理
 */
public interface ResponseInvoke {

    /**
     * 执行响应处理
     * @param requestContext
     */
    void response(RequestContext requestContext);

}
