package org.ws.httphelper.core.pipeline;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 执行排序，按照正序执行
 * sys比user优先
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HandlerOrder {

    /**
     * order值
     * @return
     */
    int value() default 0;

    /**
     * 级别。
     * 无论是RequestHandler还是ResponseHandler都是System比User级别优先执行
     * @return
     */
    OrderLevel level() default OrderLevel.USER;

    enum OrderLevel{
        SYSTEM,
        USER;
    }
}
