package org.ws.httphelper.core.pipeline;

import org.ws.httphelper.model.RequestContext;

import java.util.concurrent.ExecutorService;

/**
 * 包装HttpHandler的上下文
 */
public interface HttpHandlerContext extends RequestInvoke,ResponseInvoke{

    /**
     * 当前的Handler
     * @return
     */
    HttpHandler handler();

    /**
     * 当前Pipeline
     * @return
     */
    HttpPipeline pipeline();

    /**
     * handler对应的executor
     * @return
     */
    ExecutorService executor();

    /**
     * 执行request处理
     * @param requestContext
     */
    @Override
    void request(RequestContext requestContext);

    /**
     * 执行response处理
     * @param requestContext
     */
    @Override
    void response(RequestContext requestContext);
}
