package org.ws.httphelper.model.http;

public enum ResponseType {
    // 不做处理
    TEXT,
    // 可解析：抽取数据
    HTML,
    // 可解析为对象
    JSON,
    JAVASCRIPT,
    // 可解析为对象
    XML,
    // 可存储为文件
    BINARY;
}
