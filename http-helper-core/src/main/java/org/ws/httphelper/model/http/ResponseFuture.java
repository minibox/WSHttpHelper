package org.ws.httphelper.model.http;

public class ResponseFuture<T> {
    private RequestData requestData;
    private ResponseData responseData;
    private boolean success;
    private Throwable error;
    // 解析结果
    private T output;
    // 响应结果类型
    private ResponseType responseType;

    public ResponseFuture(RequestData requestData) {
        this.requestData = requestData;
    }

    public ResponseFuture(RequestData requestData, ResponseData responseData) {
        this.requestData = requestData;
        this.responseData = responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    public ResponseData getResponse() {
        return this.responseData;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public RequestData getRequest() {
        return this.requestData;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public T getOutput() {
        return output;
    }

    public void setOutput(T output) {
        this.output = output;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }
}
