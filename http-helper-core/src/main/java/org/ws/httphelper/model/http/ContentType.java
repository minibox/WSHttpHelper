package org.ws.httphelper.model.http;

public enum ContentType {
    EMPTY("*/*"),
    FORM_DATA("multipart/form-data"),
    X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded"),
    RAW_TEXT("text/plain"),
    RAW_JSON("application/json"),
    RAW_HTML("text/html"),
    RAW_XML("application/xml"),
    BINARY("application/octet-stream");

    private String type;

    ContentType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
