package org.ws.httphelper.model;

import com.google.common.collect.Maps;
import org.ws.httphelper.core.pipeline.HttpPipeline;
import org.ws.httphelper.model.http.RequestData;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.config.RequestConfig;

import java.util.Map;

/**
 * 每次请求对应一个Context
 */
public final class RequestContext {
    // 请求传入的URL
    private final String url;
    // 传入的参数
    private final Object input;
    // 需要解析的响应类型
    private Class outputClass;
    // 扩展数据
    private final Map<String, Object> map;
    // 对应模板
    private final RequestConfig requestConfig;
    // 生产或传入的参数
    private final Map<String, Object> params;
    // 实际生成的请求数据
    private final RequestData requestData;
    // 响应数据
    private ResponseFuture responseFuture;

    public RequestContext(String url, Object input, RequestConfig requestConfig) {
        this.url = url;
        this.input = input;
        this.requestConfig = requestConfig;

        this.map = Maps.newHashMap();
        this.params = Maps.newHashMap();
        this.requestData = new RequestData(url,requestConfig.getMethod());

        if(this.requestConfig.getContentType() != null){
            this.requestData.setContentType(this.requestConfig.getContentType());
        }
    }

    public Object getInput(){
        return input;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Object get(String key){
        return this.map.get(key);
    }

    public void put(String key, Object value){
        this.map.put(key,value);
    }

    public String getUrl() {
        return url;
    }

    public RequestData getRequestData() {
        return requestData;
    }

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public ResponseFuture getResponseFuture() {
        return responseFuture;
    }

    public void setResponseFuture(ResponseFuture responseFuture) {
        this.responseFuture = responseFuture;
    }


    public Class getOutputClass() {
        return outputClass;
    }

    public void setOutputClass(Class outputClass) {
        this.outputClass = outputClass;
    }

    public HttpPipeline getHttpPipeline() {
        return this.requestConfig.getHttpPipeline();
    }
}
