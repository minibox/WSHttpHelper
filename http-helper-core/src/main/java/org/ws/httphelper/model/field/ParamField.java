package org.ws.httphelper.model.field;

/**
 * 请求字段
 */
public class ParamField {
    /**
     * 名称
     */
    private String fieldName;

    /**
     * 默认值
     */
    private Object defaultValue;

    /**
     * 是否必须
     */
    private boolean required;

    /**
     * 字段类型
     */
    private ParamFieldType fieldType;

    /**
     * 说明
     */
    private String description;

    /**
     * 验证正则表达式
     */
    private String validation;

    /**
     * 示例
     */
    private String example;

    public ParamField(String fieldName) {
        this.fieldName = fieldName;
    }

    public ParamField(String fieldName, ParamFieldType fieldType) {
        this(fieldName,null,fieldType);
    }

    public ParamField(String fieldName, Object defaultValue, ParamFieldType fieldType) {
        this(fieldName,defaultValue,false,fieldType);
    }

    public ParamField(String fieldName, Object defaultValue, boolean required, ParamFieldType fieldType) {
        this(fieldName,defaultValue,required,fieldType,null);
    }

    public ParamField(String fieldName, Object defaultValue, boolean required, String validation) {
        this(fieldName,defaultValue,required,null,validation);
    }

    public ParamField(String fieldName, Object defaultValue, boolean required, ParamFieldType fieldType, String validation) {
        this.fieldName = fieldName;
        this.defaultValue = defaultValue;
        this.required = required;
        this.fieldType = fieldType;
        this.validation = validation;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public ParamFieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(ParamFieldType fieldType) {
        this.fieldType = fieldType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }
}
