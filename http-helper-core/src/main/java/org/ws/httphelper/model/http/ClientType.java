package org.ws.httphelper.model.http;

public enum ClientType {
    /**
     * okhttp3.OkHttpClient
     */
    OK_HTTP_CLIENT,
    /**
     * com.gargoylesoftware.htmlunit.WebClient
     */
    WEB_CLIENT
}
