package org.ws.httphelper.model.config;

import org.ws.httphelper.model.http.ClientType;

public class ClientConfig {
    // 异步请求线程
    private int threadNumber = 4;
    // 请求超时
    private int timeout = 3_000;
    // 是否启用cookie：启动后根据response的cookie更新
    private boolean enableCookie = false;
    // 请求类型：HttpClint速度快适用于接口，WebClient能够自动执行JS速度慢适用于HTML网页
    private ClientType clientType = ClientType.OK_HTTP_CLIENT;
    // 是否开启ssl
    private boolean enableSsl = true;
    // 是否允许重定向
    private boolean followRedirects = true;

    // 私有Client:true=当前HttpHelper使用自己Client;false=从FetcherPool中获取共享Client，与其他HttpHelper共享.
    private boolean privateClient = false;

    public int getThreadNumber() {
        return threadNumber;
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isEnableCookie() {
        return enableCookie;
    }

    public void setEnableCookie(boolean enableCookie) {
        this.enableCookie = enableCookie;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public boolean isEnableSsl() {
        return enableSsl;
    }

    public void setEnableSsl(boolean enableSsl) {
        this.enableSsl = enableSsl;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public boolean isPrivateClient() {
        return privateClient;
    }

    public void setPrivateClient(boolean privateClient) {
        this.privateClient = privateClient;
    }
}
