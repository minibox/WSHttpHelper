package org.ws.httphelper.builder.annotation;


import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseFieldType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ChildrenField {

    String fieldName();

    ParseFieldType fieldType() default ParseFieldType.STRING;

    String parseExpression() default "";

    ExpressionType expressionType() default ExpressionType.CSS;

}
