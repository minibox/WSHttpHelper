package org.ws.httphelper.support.parser;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.common.Constant;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.field.ParseFieldType;

import java.io.File;
import java.util.Map;

/** 
* HtmlObjectParser Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 15, 2020</pre> 
* @version 1.0 
*/ 
public class HtmlObjectParserTest {

    private static Logger log = LoggerFactory.getLogger(HtmlObjectParserTest.class.getName());

    private static final String SEARCH_LIST_HTML = "/servlet/search_list.html";

    private HtmlObjectParser htmlObjectParser;
    private String html;

    @Before
    public void before() throws Exception {
        htmlObjectParser = new HtmlObjectParser();
        String path = this.getClass().getResource(SEARCH_LIST_HTML).getPath();
        html = FileUtils.readFileToString(new File(path), Constant.UTF_8);
    }

    /**
    *
    * Method: parse(final String html, final Collection<ParseField> parseFields)
    *
    */
    @Test
    public void testHttpCssParseHtml() throws Exception {
        ParseField itemList = new ParseField("itemList", ParseFieldType.LIST,
                "#content_left .c-container", ExpressionType.CSS);
        itemList.addChildField("title","h3",ExpressionType.CSS);
        itemList.addChildField("link","h3>a@href",ExpressionType.CSS);
        itemList.addChildField("date","span.newTimeFactor_before_abs",ExpressionType.CSS);
        itemList.addChildField("abstract","div.c-abstract",ExpressionType.CSS);

        Map<String, Object> objectMap = htmlObjectParser.parse(html, Lists.newArrayList(itemList));

        log.info(JSON.toJSONString(objectMap, SerializerFeature.PrettyFormat));
    }

    @Test
    public void testXpathParseHtml() throws Exception {
        ParseField itemList = new ParseField("itemList", ParseFieldType.LIST,
                "//div[@id='content_left']/div[contains(@class,'c-container')]", ExpressionType.XPATH);
        itemList.addChildField("title","./h3",ExpressionType.XPATH);
        itemList.addChildField("link","./h3/a/@href",ExpressionType.XPATH);
        itemList.addChildField("date","(\\d{4}年\\d{1,2}月\\d{1,2}日)",ExpressionType.REGEX);
        itemList.addChildField("abstract","./div[contains(@class,'c-abstract')]",ExpressionType.XPATH);


        Map<String, Object> objectMap = htmlObjectParser.parse(html, Lists.newArrayList(itemList));

        log.info(JSON.toJSONString(objectMap, SerializerFeature.PrettyFormat));
    }

} 
