### 内置Handler

##### `RequestHandler`请求前处理

| 类                     | HandlerOrder(level,value) | 说明                                                         |
| ---------------------- | ------------------------- | ------------------------------------------------------------ |
| ValidationParamHandler | (SYSTEM,0)                | 对配置的`ParamField`进行参数验证Handler，支持是否必须和Regex验证。 |
| BuildParamHandler      | (SYSTEM,1)                | 若配置了`ParamField`根据input初始化对应的param；若没有配置`ParamField`，根据input的属性初始化param； |
| BuildUrlHandler        | (SYSTEM,2)                | 1.对rootUrl和入参进行拼接；2.对url中存在{key}的使用param进行替换生成具体url；3.对于GET或HEAD请求根据param生成key=value的url； |
| BuildHeaderHandler     | (SYSTEM,2)                | 根据default配置和request的header生成header。                 |
| BuildBodyHandler       | (SYSTEM,3)                | 对POST、PUT请求根据input和不同的请求ContentType生成对应的body。 |



##### `ResponseHandler`请求后处理

| 类                       | HandlerOrder(level,value)  | 说明                                                         |
| ------------------------ | -------------------------- | ------------------------------------------------------------ |
| ParseResponseTypeHandler | (SYSTEM,Integer.MAX_VALUE) | 根据Header的Content-Type解析响应的body类型:JSON,HTML,XML,TEXT,BINARY. |
| ParseJsonHandler         | (USER,1)                   | 将JSON解析为Map或指定对象类型.                               |
| ParseXmlHandler          | (USER,1)                   | 将XML解析为Map或指定对象类型.                                |
| ParseHtmlHandler         | (USER,1)                   | 将HTML解析为Map或指定对象类型.                               |
| ParseRedirectUrlHandler  | (USER,1)                   | 获取重定向的url.                                             |
| SaveFileHandler          | (USER,1)                   | 将BINARY保存为文件.                                          |
| ExportCsvHandler         | (USER,0)                   | 导出为vcs文件.                                               |
