package org.ws.httphelper.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.ws.httphelper.model.UserEntity;

import java.util.Map;

/**
 * 用于测试的接口
 */
@Api
@RequestMapping("/test/api")
@RestController
public class TestApiController {

    @RequestMapping(value="/get",method = {RequestMethod.GET,RequestMethod.HEAD})
    public UserEntity getMethodApiForTest(UserEntity userEntity){
        return userEntity;
    }

    @RequestMapping(value = "/post",method = {RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
    public Map<String,String> postgetMethodApiForTest(Map<String,String> param){
        return param;
    }
}
