package org.ws.httphelper.example.simple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.ws.httphelper.HttpHelper;
import org.ws.httphelper.builder.HttpHelperBuilder;
import org.ws.httphelper.template.DefaultRequestTemplate;

/**
 * 简单Builder示例
 */
@Configuration
public class SimpleBuilderExamples {

    @Bean
    public HttpHelper defaultTemplateHttpHelper(){
        return HttpHelperBuilder.builder()
                .builderByTemplate(DefaultRequestTemplate.okHttpGetHtml());
    }

}
