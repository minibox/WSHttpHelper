package org.ws.httphelper.example.openapi;

import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpClient;
import org.ws.httphelper.builder.annotation.HttpOperation;
import org.ws.httphelper.builder.annotation.HttpRequest;
import org.ws.httphelper.builder.annotation.RequestParam;
import org.ws.httphelper.model.http.ClientType;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseType;

import java.util.Map;

/**
 * 说明文档:http://lbsyun.baidu.com/index.php?title=webapi/guide/webservice-placeapi
 * 根据API接口的说明文档定义API请求接口
 */
@HttpClient(
        clientType = ClientType.OK_HTTP_CLIENT,
        privateClient = true,
        timeout = 3_000, threadNumber = 4,
        enableCookie = false,followRedirects = false,enableSsl = false
)
@HttpRequest(
        rootUrl = "http://api.map.baidu.com/place/v2",
        contentType = ContentType.X_WWW_FORM_URLENCODED,
        responseType = ResponseType.JSON,
        method = HttpMethod.GET
)
@HttpOperation
public interface BaiduMapSearchApi {

    /**
     * 使用注解指定请求和响应实体,参数为实体
     * @param path 请求路径
     * @param param 参数实体类
     * @return 响应的实体
     */
    @Get(
            requestEntity = SearchRegionParam.class,responseEntity = SearchRegionResult.class,
            handlers = {BuildSnHandler.class}
    )
    SearchRegionResult searchRegionByEntity(String path, SearchRegionParam param);

    /**
     * 使用注解配置请求,参数为Map,响应为泛型
     * @param path 请求路径
     * @param map Map参数
     * @param outputClass 响应的类
     * @param <T> 响应的类型:可以为Map或JSON对应的实体类
     * @return
     */
    @Get(
        requestParams = {
                @RequestParam(fieldName = "query",required = true,example = "天安门、美食",description = "检索关键字"),
                @RequestParam(fieldName = "tag",example = "美食",description = "检索分类偏好"),
                @RequestParam(fieldName = "region",required = true,example = "北京、131（北京的code）、海淀区、全国，等",description = "检索行政区划区域"),
                @RequestParam(fieldName = "city_limit",defaultValue = "true",example = "true、false",description = "区域数据召回限制，为true时，仅召回region对应区域内数据。"),
                @RequestParam(fieldName = "extensions_adcode",defaultValue = "true",example = "true、false",description = "是否召回国标行政区划编码，"),
                @RequestParam(fieldName = "output",defaultValue = "json",example = "json或xml",description = "输出格式为json或者xml"),
                @RequestParam(fieldName = "scope",defaultValue = "1",example = "1、2",description = "检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息"),
                @RequestParam(fieldName = "filter",example = "sort_name:distance|sort_rule:1",description = "检索过滤条件。当scope取值为2时，可以设置filter进行排序。"),
                @RequestParam(fieldName = "coord_type",defaultValue = "3",example = "1、2、3(默认)、4",description = "坐标类型，1（wgs84ll即GPS经纬度），2（gcj02ll即国测局经纬度坐标），3（bd09ll即百度经纬度坐标），4（bd09mc即百度米制坐标）"),
                @RequestParam(fieldName = "ret_coordtype",example = "gcj02ll",description = "可选参数，添加后POI返回国测局经纬度坐标"),
                @RequestParam(fieldName = "page_size",defaultValue = "10",example = "10",description = "单次召回POI数量，默认为10条记录，最大返回20条。多关键字检索时，返回的记录数为关键字个数*page_size。"),
                @RequestParam(fieldName = "page_num",defaultValue = "0",example = "0、1、2",description = "分页页码，默认为0,0代表第一页，1代表第二页，以此类推。"),
                @RequestParam(fieldName = "ak",required = true,description = "开发者的访问密钥，必填项。v2之前该属性为key。"),
                @RequestParam(fieldName = "sn",description = "开发者的权限签名。"),
                @RequestParam(fieldName = "timestamp",description = "设置sn后该值必填。")
        },
        handlers = {BuildSnHandler.class}
    )
    <T> T searchRegionByMap(String path, Map<String,String> map, Class<T> outputClass);
}
