package org.ws.httphelper.example.annotation;

import org.ws.httphelper.builder.annotation.Get;
import org.ws.httphelper.builder.annotation.HttpClient;
import org.ws.httphelper.builder.annotation.HttpOperation;
import org.ws.httphelper.model.http.ClientType;

/**
 * 声明一个接口,使用@HttpOperation注解
 */
@HttpClient(
        clientType = ClientType.WEB_CLIENT, timeout = 10_000,
        enableSsl = true, enableCookie = true, followRedirects = true,
        privateClient = true
)
@HttpOperation
public interface TopBaidu {

    /**
     * 使用@Get(value=rootUrl),指定响应类型
     * @return
     */
    @Get(
            value = "http://top.baidu.com/buzz?b=1&fr=topindex",
            responseEntity = TopNews.class,
            charset = "GBK"
    )
    TopNews getTopNews();

    /**
     * 使用@Get,指定响应类型;通过传入url执行
     * @return
     */
    @Get(
            responseEntity = TopNews.class,
            charset = "GBK"
    )
    TopNews getTopNewsByUrl(String url);
}
